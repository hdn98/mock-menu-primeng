import { MenuItem } from 'primeng/api';
import { Pipe, PipeTransform } from '@angular/core';
import { IBreadCrumb } from '../models/app-config.model';
import { cloneDeep } from 'lodash';
@Pipe({
  name: 'breadcrumbPipeTs',
  standalone: true,
})
export class BreadcrumbPipeTsPipe implements PipeTransform {
  transform(value: IBreadCrumb[]) {
    const newList: IBreadCrumb[] = [];
    let newRoute = '';
    value.reverse().forEach((item, index) => {
      if (item.link !== '') {
        newRoute += '/' + item.link;
        item.link = newRoute;
        newList.push(item);
      }
    });
    console.log(newList);
    return [...newList];
  }
}
