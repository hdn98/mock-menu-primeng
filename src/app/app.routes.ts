import { Route } from '@angular/router';
import { AgencyInfoComponent } from './pages/agency-manager/agency-info/agency-info.component';
import { AgencyLeadComponent } from './pages/agency-bancheo/agency-lead/agency-lead.component';
import { MappingGroupComponent } from './pages/agency-bancheo/mapping-group/mapping-group.component';
import { AgencyGroupComponent } from './pages/agency-bancheo/agency-group/agency-group.component';
import { ApproveChangeComponent } from './pages/agency-manager/approve-change/approve-change.component';
import { ApproveAgreementComponent } from './pages/agency-manager/approve-agreement/approve-agreement.component';
import { ApproveNewComponent } from './pages/agency-manager/approve-new/approve-new.component';
import { PersonComponent } from './pages/training-manager/person/person.component';
import { LearnerTrainerComponent } from './pages/training-manager/learner-trainer/learner-trainer.component';

import { UpdateComponent as PersonUpdateComponent } from './pages/training-manager/person/update/update.component';
import { UpdateComponent as AgencyInfoUpdateComponent } from './pages/agency-manager/agency-info/update/update.component';
import { UpdateComponent as MappingGroupUpdateComponent } from './pages/agency-bancheo/mapping-group/update/update.component';
import { UpdateComponent as ProgramUpdateComponent } from './pages/training-manager/program/update/update.component';
import { UpdateComponent as CourseUpdateComponent } from './pages/training-manager/course/update/update.component';

import { DetailComponent as AgencyInfoDetailComponent } from './pages/agency-manager/agency-info/detail/detail.component';
import { DetailComponent as AgencyLeadDetailComponent } from './pages/agency-bancheo/agency-lead/detail/detail.component';
import { DetailComponent as MappingGroupDetailComponent } from './pages/agency-bancheo/mapping-group/detail/detail.component';
import { DetailComponent as TVVDetailComponent } from './pages/agency-bancheo/tvv/detail/detail.component';
import { DetailComponent as ApproveAgreementDetailComponent } from './pages/agency-manager/approve-agreement/detail/detail.component';
import { DetailComponent as PersonDetailComponent } from './pages/training-manager/person/detail/detail.component';
import { DetailComponent as ProgramDetailComponent } from './pages/training-manager/program/detail/detail.component';
import { DetailComponent as CourseDetailComponent } from './pages/training-manager/course/detail/detail.component';
import { ApproveDetailComponent } from './pages/agency-manager/agency-info/approve-detail/approve-detail.component';
import { TvvComponent } from './pages/agency-bancheo/tvv/tvv.component';
import { CertficateTvvComponent } from './pages/agency-bancheo/certficate-tvv/certficate-tvv.component';
import { BankUpdateComponent } from './pages/agency-bancheo/bank-update/bank-update.component';
import { ProgramComponent } from './pages/training-manager/program/program.component';
import { CourseComponent } from './pages/training-manager/course/course.component';

type IRoute = Omit<Route, 'data'> & {
  data?: {
    breadcrumb?: string;
  };
};

export const routes: IRoute[] = [
  {
    path: '',
    redirectTo: 'agency-manager/manager',
    pathMatch: 'full',
  },
  {
    path: 'training-manager',
    data: {
      breadcrumb: 'Quản lí đối tượng và đào tạo',
    },
    children: [
      {
        path: '',
        redirectTo: 'manager',
        pathMatch: 'prefix',
      },
      {
        path: 'manager',
        data: {
          breadcrumb: 'Quản lí đối tượng',
        },
        children: [
          {
            path: '',
            redirectTo: 'person',
            pathMatch: 'prefix',
          },

          {
            path: 'person',
            data: {
              breadcrumb: 'Danh sách đối tượng',
            },
            children: [
              {
                path: '',
                component: PersonComponent,
              },
              {
                path: 'update',
                component: PersonUpdateComponent,
                data: {
                  breadcrumb: 'Cập nhật thông tin đối tượng',
                },
              },
              {
                path: 'create',
                component: PersonUpdateComponent,
                data: {
                  breadcrumb: 'Thêm mới đối tượng',
                },
              },
              {
                path: 'detail',
                component: PersonDetailComponent,
                data: {
                  breadcrumb: 'Xem chi tiết đối tượng',
                },
              },
            ],
          },
          {
            path: 'trainer',
            data: {
              breadcrumb: 'Danh sách giảng viên',
            },
            children: [
              {
                path: '',
                component: LearnerTrainerComponent,
              },
              {
                path: 'update',
                component: PersonUpdateComponent,
                data: {
                  breadcrumb: 'Cập nhật thông tin giảng viên',
                },
              },
              {
                path: 'detail',
                component: PersonDetailComponent,
                data: {
                  breadcrumb: 'Xem chi tiết giảng viên',
                },
              },
            ],
          },
          {
            path: 'learner',
            data: {
              breadcrumb: 'Danh sách học viên',
            },
            children: [
              {
                path: '',
                component: LearnerTrainerComponent,
              },
              {
                path: 'update',
                component: PersonUpdateComponent,
                data: {
                  breadcrumb: 'Cập nhật thông tin học viên',
                },
              },
              {
                path: 'detail',
                component: PersonDetailComponent,
                data: {
                  breadcrumb: 'Xem chi tiết thông tin học viên',
                },
              },
            ],
          },
        ],
      },
      {
        path: 'program',
        data: {
          breadcrumb: 'Chương trình đào tạo',
        },
        children: [
          {
            path: '',
            component: ProgramComponent,
          },
          {
            path: 'update',
            component: ProgramUpdateComponent,
            data: {
              breadcrumb: 'Thêm mới chương trình đào tạo',
            },
          },
          {
            path: 'detail',
            component: ProgramDetailComponent,
            data: {
              breadcrumb: 'Xem chi tiết chương trình đào tạo',
            },
          },
        ],
      },
      {
        path: 'course',
        data: {
          breadcrumb: 'Khóa đào tạo',
        },
        children: [
          {
            path: '',
            component: CourseComponent,
          },
          {
            path: 'update',
            component: CourseUpdateComponent,
            data: {
              breadcrumb: 'Thêm mới khóa đào tạo',
            },
          },
          {
            path: 'detail',
            component: CourseDetailComponent,
            data: {
              breadcrumb: 'Xem chi tiết khóa đào tạo',
            },
          },
        ],
      },
    ],
  },
  {
    path: 'agency-manager',
    data: {
      breadcrumb: 'Quản lý đại lý cá nhân',
    },
    children: [
      {
        path: '',
        redirectTo: 'agency-info',
        pathMatch: 'prefix',
      },
      {
        path: 'agency-info',
        data: {
          breadcrumb: 'Đại lý cá nhân',
        },
        children: [
          {
            path: '',
            component: AgencyInfoComponent,
            children: [
              {
                path: 'update',
                component: AgencyInfoUpdateComponent,
                data: {
                  breadcrumb: 'Cập nhật thông tin đại lý trước phê duyệt',
                },
              },
              { path: 'detail', component: AgencyInfoDetailComponent },
              {
                path: 'update-ref',
                component: AgencyInfoUpdateComponent,
                data: {
                  breadcrumb: 'Chỉnh sửa thông tin đại lý',
                },
              },
            ],
          },
        ],
      },
      {
        path: 'approve-change',
        data: {},
        children: [
          {
            path: '',
            component: ApproveChangeComponent,
          },
          {
            path: 'detail',
            component: ApproveDetailComponent,
          },
        ],
      },
      {
        path: 'approve-agreement',

        children: [
          {
            path: '',
            component: ApproveAgreementComponent,
          },
          {
            path: 'detail',
            component: ApproveAgreementDetailComponent,
          },
        ],
      },
      {
        path: 'agency-approve-new',

        children: [
          {
            path: '',
            component: ApproveNewComponent,
          },
        ],
      },
    ],
  },

  {
    path: 'group-manager',

    children: [
      {
        path: '',
        redirectTo: 'agency-lead',
        pathMatch: 'full',
      },
      {
        path: 'agency-lead',

        children: [
          {
            path: '',
            component: AgencyLeadComponent,
          },
          {
            path: 'detail',
            component: AgencyLeadDetailComponent,
          },
        ],
      },
      {
        path: 'mapping-group',

        children: [
          {
            path: '',
            component: MappingGroupComponent,
          },
          {
            path: 'update',
            component: MappingGroupUpdateComponent,
          },
          {
            path: 'detail',
            component: MappingGroupDetailComponent,
          },
        ],
      },
      {
        path: 'agency-group',

        children: [
          {
            path: '',
            component: AgencyGroupComponent,
          },
        ],
      },
      {
        path: 'certificate-tvv',

        children: [
          {
            path: '',
            component: CertficateTvvComponent,
          },
        ],
      },
      {
        path: 'tvv',

        children: [
          {
            path: '',
            component: TvvComponent,
          },
          {
            path: 'detail',
            component: TVVDetailComponent,
          },
        ],
      },
      {
        path: 'bank-update',

        children: [
          {
            path: '',
            component: BankUpdateComponent,
          },
        ],
      },
    ],
  },
];
