import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BreadcrumbService } from '../../../../../../service/breadcrumb.service';
import { ROUTES_CONFIG } from '../../../../shared/config-route';

@Component({
  selector: 'app-update',
  standalone: true,
  imports: [],
  templateUrl: './update.component.html',
  styleUrl: './update.component.scss',
})
export class UpdateComponent {
  constructor(
    public router: Router,
    private breadCrumbSerivce: BreadcrumbService
  ) {}
  backToList() {
    this.router.navigate([ROUTES_CONFIG.ROUTE_LIST_COURSE]);
  }
}
