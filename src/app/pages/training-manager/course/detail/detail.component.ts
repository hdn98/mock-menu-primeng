import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTES_CONFIG } from '../../../../shared/config-route';

@Component({
  selector: 'app-detail',
  standalone: true,
  imports: [],
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.scss'
})
export class DetailComponent {
  constructor(private router: Router) {}
  backToList() {
    this.router.navigate([ROUTES_CONFIG.ROUTE_LIST_COURSE]);
  }
}
