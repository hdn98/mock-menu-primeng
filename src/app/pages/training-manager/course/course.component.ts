import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-course',
  standalone: true,
  imports: [],
  templateUrl: './course.component.html',
  styleUrl: './course.component.scss',
})
export class CourseComponent {
  constructor(public router: Router) {}
  routerEdit() {
    this.router.navigate(['training-manager/course/update']);
  }
  routerDetail() {
    this.router.navigate(['training-manager/course/detail']);
  }
}
