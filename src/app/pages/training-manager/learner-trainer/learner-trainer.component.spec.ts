import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnerTrainerComponent } from './learner-trainer.component';

describe('LearnerTrainerComponent', () => {
  let component: LearnerTrainerComponent;
  let fixture: ComponentFixture<LearnerTrainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LearnerTrainerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(LearnerTrainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
