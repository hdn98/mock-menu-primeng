import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { tap } from 'rxjs';
import { BreadcrumbService } from '../../../../../service/breadcrumb.service';

@Component({
  selector: 'app-learner-trainer',
  standalone: true,
  imports: [],
  templateUrl: './learner-trainer.component.html',
  styleUrl: './learner-trainer.component.scss'
})
export class LearnerTrainerComponent {
  
  routeParam = '';
  constructor(public router:Router, public activateRoute : ActivatedRoute,
    private breadCrumbService: BreadcrumbService
  ){
    // this.router.events.subscribe((event)=>{
    //   if(event instanceof NavigationEnd){
    //     console.log(event.url)
    //   }
    // })
    this.routeParam = this.breadCrumbService.getlastPageCaseString()
  }
  routerEdit(){
    this.router.navigate([`training-manager/manager/${this.routeParam}/update`]);
  }
  routerDetail(){
    this.router.navigate([`training-manager/manager/${this.routeParam}/detail`]);
  }

}
