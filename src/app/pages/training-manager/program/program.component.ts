import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-program',
  standalone: true,
  imports: [],
  templateUrl: './program.component.html',
  styleUrl: './program.component.scss'
})
export class ProgramComponent {

  constructor(public router: Router) {}
  ngOnInit(): void {}
  routerEdit() {
    this.router.navigate(["training-manager/program/update"]);
  }
  routerDetail() {
    this.router.navigate(["training-manager/program/detail"]);
  }
}
