import { Component } from '@angular/core';
import { ROUTES_CONFIG } from '../../../../shared/config-route';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detail',
  standalone: true,
  imports: [],
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.scss',
})
export class DetailComponent {
  constructor(private router: Router) {}
  backToList() {
    this.router.navigate([ROUTES_CONFIG.ROUTE_LIST_PROGRAM]);
  }
}
