import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-person",
  standalone: true,
  imports: [],
  templateUrl: "./person.component.html",
  styleUrl: "./person.component.scss",
})
export class PersonComponent implements OnInit {
  constructor(public router: Router) {}
  ngOnInit(): void {}
  routerEdit() {
    this.router.navigate(["training-manager/manager/person/update"]);
  }
  routerDetail() {
    this.router.navigate(["training-manager/manager/person/detail"]);
  }
  routerCreate(){
    this.router.navigate(["training-manager/manager/person/create"]);
  }
}
