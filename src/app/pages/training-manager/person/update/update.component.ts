import { Router } from "@angular/router";
import { Component } from "@angular/core";
import { ROUTES_CONFIG } from "../../../../shared/config-route";
import { BreadcrumbService } from "../../../../../../service/breadcrumb.service";

@Component({
  selector: "app-update",
  standalone: true,
  imports: [],
  templateUrl: "./update.component.html",
  styleUrl: "./update.component.scss",
})
export class UpdateComponent {
  pageCase = "";
  constructor(
    public router: Router,
    private breadCrumbSerivce: BreadcrumbService
  ) {
    const url = this.router.url;
    if (url.includes("learner")) {
      this.pageCase = "learner";
    }
    if (url.includes("trainer")) {
      this.pageCase = "trainer";
    }
    if (url.includes("person")) {
      this.pageCase = "person";
    }
  }
  backToList() {
    if (this.pageCase === "learner") {
      this.router.navigate([ROUTES_CONFIG.ROUTE_LIST_LEARNER]);
    }
    if (this.pageCase === "trainer") {
      this.router.navigate([ROUTES_CONFIG.ROUTE_LIST_TRAINER]);
    }
    if (this.pageCase === "person") {
      this.router.navigate([ROUTES_CONFIG.ROUTE_LIST_PERSON]);
    }
  }
}
