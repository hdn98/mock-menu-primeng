import { Component } from '@angular/core';
import {  Router } from '@angular/router';
import { ROUTES_CONFIG } from '../../../../shared/config-route';

@Component({
  selector: 'app-detail',
  standalone: true,
  imports: [],
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.scss'
})
export class DetailComponent {
  pageCase = '';
  constructor(private router : Router){
    const url = this.router.url;
    if (url.includes("learner")) {
      this.pageCase = "learner";
    }
    if (url.includes("trainer")) {
      this.pageCase = "trainer";
    }
    if (url.includes("person")) {
      this.pageCase = "person";
    }
  }
  backToList(){
    if (this.pageCase === "learner") {
      this.router.navigate([ROUTES_CONFIG.ROUTE_LIST_LEARNER]);
    }
    if (this.pageCase === "trainer") {
      this.router.navigate([ROUTES_CONFIG.ROUTE_LIST_TRAINER]);
    }
    if (this.pageCase === "person") {
      this.router.navigate([ROUTES_CONFIG.ROUTE_LIST_PERSON]);
    }
  }
}
