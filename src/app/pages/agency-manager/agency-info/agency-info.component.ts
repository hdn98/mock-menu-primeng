import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-agency-info',
  standalone: true,
  imports: [],
  templateUrl: './agency-info.component.html',
  styleUrl: './agency-info.component.scss',
})
export class AgencyInfoComponent {
  constructor(public router: Router) {}
  ngOnInit(): void {}
  routerEdit() {
    this.router.navigate(['agency-manager/agency-info/update']);
  }
  routerDetail() {
    this.router.navigate(['agency-manager/agency-info/detail']);
  }
}
