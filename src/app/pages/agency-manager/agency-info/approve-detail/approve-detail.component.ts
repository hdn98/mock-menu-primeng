import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BreadcrumbService } from '../../../../../../service/breadcrumb.service';
import { ROUTES_CONFIG } from '../../../../shared/config-route';

@Component({
  selector: 'app-approve-detail',
  standalone: true,
  imports: [],
  templateUrl: './approve-detail.component.html',
  styleUrl: './approve-detail.component.scss',
})
export class ApproveDetailComponent {
  
  constructor(
    public router: Router,
    private breadCrumbSerivce: BreadcrumbService
  ) {}

  backToList() {
    this.router.navigate([ROUTES_CONFIG.ROUTE_LIST_PROGRAM]);
  }
}
