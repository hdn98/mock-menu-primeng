import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveNewComponent } from './approve-new.component';

describe('ApproveNewComponent', () => {
  let component: ApproveNewComponent;
  let fixture: ComponentFixture<ApproveNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ApproveNewComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ApproveNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
