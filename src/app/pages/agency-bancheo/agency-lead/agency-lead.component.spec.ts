import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgencyLeadComponent } from './agency-lead.component';

describe('AgencyLeadComponent', () => {
  let component: AgencyLeadComponent;
  let fixture: ComponentFixture<AgencyLeadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AgencyLeadComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AgencyLeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
