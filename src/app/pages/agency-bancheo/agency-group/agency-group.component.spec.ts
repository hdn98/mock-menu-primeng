import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgencyGroupComponent } from './agency-group.component';

describe('AgencyGroupComponent', () => {
  let component: AgencyGroupComponent;
  let fixture: ComponentFixture<AgencyGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AgencyGroupComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AgencyGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
