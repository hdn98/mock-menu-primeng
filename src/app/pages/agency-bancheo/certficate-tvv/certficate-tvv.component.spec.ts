import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CertficateTvvComponent } from './certficate-tvv.component';

describe('CertficateTvvComponent', () => {
  let component: CertficateTvvComponent;
  let fixture: ComponentFixture<CertficateTvvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CertficateTvvComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CertficateTvvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
