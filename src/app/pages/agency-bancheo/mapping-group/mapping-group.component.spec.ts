import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingGroupComponent } from './mapping-group.component';

describe('MappingGroupComponent', () => {
  let component: MappingGroupComponent;
  let fixture: ComponentFixture<MappingGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MappingGroupComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MappingGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
