import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import {
  ActivatedRoute,
  ActivationEnd,
  ActivationStart,
  ChildActivationEnd,
  NavigationEnd,
  NavigationStart,
  Router,
  RouterOutlet,
  RoutesRecognized,
} from '@angular/router';
import { NavbarComponent } from '../layout/navbar/navbar.component';
import { SidebarComponent } from '../layout/sidebar/sidebar.component';
import { MainPageComponent } from '../layout/main-page/main-page.component';
import { BreadcrumbService } from '../../service/breadcrumb.service';
import {
  BehaviorSubject,
  Subject,
  combineLatest,
  distinctUntilChanged,
  last,
  tap,
} from 'rxjs';
import { IBreadCrumb } from './models/app-config.model';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NavbarComponent, SidebarComponent, MainPageComponent],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  title = 'mock-router';
  constructor(
    private router: Router,
    private breadcrumbService: BreadcrumbService,

    public activatedRoute: ActivatedRoute
  ) {}
  routerList = new BehaviorSubject<IBreadCrumb[]>([]);
  mappingAllRouterConfig(config: any) {
    console.log(config);
  }
  ngOnInit() {
    this.router.events
      .pipe(
        tap((ev) => {
          if (ev instanceof NavigationStart) {
            this.routerList.next([]);
          }
          if (ev instanceof ActivationEnd) {
            // lấy all router data theo router config
            const breadcrumbData: IBreadCrumb = {
              label: (ev.snapshot.data as any)?.breadcrumb ?? '',
              link: ev.snapshot.routeConfig?.path ?? '',
            };

            if (breadcrumbData) {
              this.routerList.next([...new Set(this.routerList.value), breadcrumbData]);
            }
          }
          if (ev instanceof NavigationEnd) {
            this.breadcrumbService.setBreadcrumbData(ev.urlAfterRedirects);
            // console.log(ev.urlAfterRedirects);
          }
        })
      )
      .subscribe();

    this.routerList.pipe(distinctUntilChanged()).subscribe((listData) => {
      console.log(listData);
    });
  }
}
