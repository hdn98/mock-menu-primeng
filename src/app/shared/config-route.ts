export const ROUTES_CONFIG = {
  ROUTE_LIST_PERSON: 'training-manager/manager/person',
  ROUTE_LIST_LEARNER: '/training-manager/manager/learner',
  ROUTE_LIST_TRAINER: '/training-manager/manager/trainer',

  ROUTE_LIST_AGENCY: 'agency-manager/agency-info',
  ROUTE_LIST_AGENCY_ADDNEW: '/agency-manager/agency-approve-new',
  ROUTE_LIST_AGENCY_AGREEMENT: '/agency-manager/approve-agreement',
  ROUTE_LIST_AGENCY_APPROVE_CHANGE: '/agency-manager/approve-change',

  ROUTE_LIST_GROUP_MANAGER_TVV: '/group-manager/tvv',
  ROUTE_LIST_GROUP_MANAGER: '/group-manager/agency-group',
  ROUTE_LIST_MAPPING_GROUP: '/group-manager/mapping-group',
  ROUTE_LIST_AGENCY_LEAD: '/group-manager/agency-lead',
  ROPUTE_LIST_AGENCY_TVV_CERTIFICATE: '/group-manager/certificate-tvv',
  ROUTE_LIST_AGENCY_BANK_UPDATE: '/group-manager/bank-update',

  ROUTE_LIST_PROGRAM: '/training-manager/program',
  ROUTE_LIST_COURSE: '/training-manager/course',
};
