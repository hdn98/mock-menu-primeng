export interface IBreadCrumb {
  label: string;
  link: string;
}
