import { Component, Input, OnInit, inject } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { IBreadCrumb } from '../../app/models/app-config.model';
import { BreadcrumbPipeTsPipe } from '../../app/pipe/breadcrumb.pipe';

@Component({
  selector: 'app-navbar[breadcrumbData]',
  standalone: true,
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss',
  imports: [BreadcrumbPipeTsPipe, RouterModule],
})
export class NavbarComponent implements OnInit {
  @Input() breadcrumbData: IBreadCrumb[] = [];

  ngOnInit(): void {}
}
