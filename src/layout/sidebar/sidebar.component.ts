import { CommonModule, NgForOf, NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavigationEnd, Router, RouterLink } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { DropdownModule } from 'primeng/dropdown';
import { Menu, MenuModule } from 'primeng/menu';
import { BreadcrumbService } from '../../../service/breadcrumb.service';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [MenuModule, NgForOf, RouterLink, NgIf, CommonModule],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.scss',
})
export class SidebarComponent implements OnInit {
  constructor(
    private router: Router,
    private breadCrumbService: BreadcrumbService
  ) {}
  ngOnInit(): void {
    this.breadCrumbService.getBreadcrumbData().subscribe((url) => {
      const urlString = url.join('/');
      this.mappingMenuItemWithUrlCurrent(urlString, this.menuItems);
    });
  }

  mappingMenuItemWithUrlCurrent(url: string, node: MenuItem[]): MenuItem[] {
    return node?.map((nodeItem: MenuItem) => {
      nodeItem.styleClass = url.includes(nodeItem.routerLink)
        ? 'menu-active'
        : '';
      if (nodeItem.items) {
        nodeItem.items = this.mappingMenuItemWithUrlCurrent(url, [
          ...nodeItem.items,
        ]);
      }
      return nodeItem;
    });
  }

  menuItems: MenuItem[] = [
    {
      label: 'Quản lí đối tượng và đào tạo ',
      icon: 'pi pi-fw pi-home',
      routerLink: 'training-manager',
      items: [
        {
          label: 'Quản lý đối tượng',
          icon: 'pi pi-fw pi-home',
          routerLink: '/training-manager/manager',
          items: [
            {
              label: 'Danh sách đối tượng',
              icon: 'pi pi-fw pi-home',
              routerLink: '/training-manager/manager/person',
            },
            {
              label: 'Học viên',
              icon: 'pi pi-fw pi-home',
              routerLink: '/training-manager/manager/learner',
            },
            {
              label: 'Giảng viên',
              icon: 'pi pi-fw pi-home',
              routerLink: '/training-manager/manager/trainer',
            },
          ],
        },
        {
          label: 'Chương trình đào tạo ',
          icon: 'pi pi-fw pi-home',
          routerLink:'/training-manager/program'
        },
        {
          label: 'Khóa học',
          icon: 'pi pi-fw pi-home',
          routerLink:'/training-manager/course'
        },
      ],
    },
    {
      label: 'Quản lý đại lý cá nhân',
      icon: 'pi pi-fw pi-shopping-cart',
      routerLink: 'agency-manager',
      items: [
        {
          label: 'Đại lý cá nhân',
          icon: 'pi pi-fw pi-plus',
          routerLink: 'agency-manager/agency-info',
          items: [
            {
              label: 'Tất cả đại lý ',
              icon: 'pi pi-fw pi-plus',
              routerLink: 'agency-manager/agency-info',
            },
            {
              label: 'Đại lý chưa kích hoạt',
              icon: 'pi pi-fw pi-plus',
              // routerLink: '/dashboard',
            },
          ],
        },
        {
          label: 'Phê duyệt cấp mới',
          icon: 'pi pi-fw pi-list',
          routerLink: '/agency-manager/agency-approve-new',
        },
        {
          label: 'Phê duyệt thỏa thuận',
          icon: 'pi pi-fw pi-list',
          routerLink: '/agency-manager/approve-agreement',
        },
        {
          label: 'Phê duyệt thông tin ',
          icon: 'pi pi-fw pi-list',
          routerLink: '/agency-manager/approve-change',
        },
      ],
    },
    {
      label: 'Quản lý đại lý bán chéo',
      icon: 'pi pi-fw pi-shopping-cart',
      routerLink: 'group-manager',
      items: [
        {
          label: 'Tư vấn viên nhân thọ',
          icon: 'pi pi-fw pi-plus',
          routerLink: '/group-manager/tvv',
        },
        {
          label: 'Ban nhóm nhân thọ',
          icon: 'pi pi-fw pi-plus',
          routerLink: '/group-manager/agency-group',
        },
        {
          label: 'Mapping ban nhóm nhân thọ ',
          icon: 'pi pi-fw pi-plus',
          routerLink: '/group-manager/mapping-group',
        },
        {
          label: 'Quản lý đầu mối ',
          icon: 'pi pi-fw pi-plus',
          routerLink: '/group-manager/agency-lead',
        },
        {
          label: 'Chứng chỉ tư vấn viên',
          icon: 'pi pi-fw pi-plus',
          routerLink: '/group-manager/certificate-tvv',
        },
        {
          label: 'Cập nhật thông tin ngân hàng ',
          icon: 'pi pi-fw pi-plus',
          routerLink: '/group-manager/bank-update',
        },
      ],
    },
    // Add more menu items and sub-menu items as needed
  ];
  redirectUrl(routerPath: string) {
    if (routerPath) {
      this.router.navigateByUrl(routerPath);
    }
  }
}
