import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BreadcrumbService {
  private breadcrumbData = new BehaviorSubject<string[]>([]);
  private routerUrlData = new BehaviorSubject<string[]>([]);
  constructor(private router: Router, private route: ActivatedRoute) {}
  
  setBreadcrumbData(url: string) {
    const listUrlSplit = url.split('/');
    this.breadcrumbData.next(listUrlSplit);
  }



  getBreadcrumbData() {
    return this.breadcrumbData.asObservable();
  }

  getlastPageCaseString() {
    return this.router.url.substring(
      this.router.url.lastIndexOf('/') + 1,
      this.router.url.length
    );
  }

  private generateBreadcrumbData(
    route: ActivatedRoute,
    url: string = '',
    breadcrumbs: string[] = []
  ): any {
    const children = route.children;

    if (children.length === 0) {
      return breadcrumbs;
    }

    for (const child of children) {
      const routeUrl = child.snapshot.url
        .map((segment) => segment.path)
        .join('/');
      if (routeUrl !== '') {
        url += `/${routeUrl}`;
      }

      const label = child.snapshot.data['breadcrumb'] || '';
      if (label !== '') {
        breadcrumbs.push({ url, label } as any);
      }

      return this.generateBreadcrumbData(child, url, breadcrumbs);
    }
  }
}
